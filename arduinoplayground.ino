#include <Arduino.h>

//pins

const int digitalTouchPin = 2;
const int digitalOutPin = 6;

int sensorVal = 0;
int outputVal = 0;

void setup() {
    // serial comms @ 115200bps
    Serial.begin(115200);

    // led feedback if program is running
    pinMode(LED_BUILTIN, OUTPUT);

    pinMode(digitalOutPin, OUTPUT);    

}

void loop() {

    sensorVal = digitalRead(digitalTouchPin);

    //map analog in val to range of out val
    outputVal = map(sensorVal, 0, 1023, 0, 255);

    if (sensorVal == 1)
    {
        Serial.println("1");
        digitalWrite(digitalOutPin, HIGH);
        delay(100);
        digitalWrite(digitalOutPin, LOW);
        delay(100);


    }
    else{
        delay(200);
        Serial.println("0");
    }
    
}
